module V1
  class AuthenticationTokensEndpoint < BaseEndpoint
    resource :authentication_tokens do
      desc 'Create a token.'
      params do
        requires :email
        requires :password
      end
      post '/' do
        user = User.find_for_authentication(email: params[:email])
        if user && user.valid_password?(params[:password])
          user.authentication_tokens.create
        else
          error! 'Invalid login credentials.', 401
        end
      end

      desc 'Revoke all tokens.'
      delete '/' do
        authenticate_user!
        current_user.authentication_tokens.destroy_all
      end

      group '/:id' do
        before do
          authenticate_user!
          @authentication_token = current_user.authentication_token.find(params[:id])
        end

        desc 'Extend the expiration time of a token'
        post '/refresh' do
          @authentication_token.refresh!
          respond_with @authentication_token
        end

        desc 'Revoke a token.'
        delete '/' do
          @authentication_token.destroy
          respond_with @authentication_token
        end
      end
    end
  end
end