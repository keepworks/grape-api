module V1
  class ContactsEndpoint < BaseEndpoint
    resource :contacts do
      desc 'Create new contact'
      params do
        requires :contact, type: Hash do
          requires :first_name, type: String
          optional :last_name, type: String
          optional :dob, type: Date
          optional :picture, type: Rack::Multipart::UploadedFile

          optional :email_addresses_attributes, type: Array do
            requires :value, type: String
            requires :kind, type: String, values: Contactable::KINDS, default: Contactable::KINDS[0]
          end

          optional :phone_numbers_attributes, type: Array do
            requires :country_code, type: String
            requires :number, type: String
            requires :kind, type: String, values: Contactable::KINDS, default: Contactable::KINDS[0]
          end

          optional :postal_addresses_attributes, type: Array do
            requires :address_line1, type: String
            optional :address_line2, type: String
            requires :city, type: String
            requires :state, type: String
            requires :country, type: String
            requires :postal_code, type: String
            requires :kind, type: String, values: Contactable::KINDS, default: Contactable::KINDS[0]
          end
        end
      end
      post '/', serializer: DetailedContactSerializer do
        p clean_params
        p ContactParameters.new(clean_params).permit
        contact = current_user.contacts.new(ContactParameters.new(clean_params).permit)
        contact.picture = params[:contact][:picture]
        contact.save
        respond_with contact
      end

      desc 'Fetch all contacts'
      get '/' do
        paginate current_user.contacts
      end

      group '/:id' do
        before do
          @contact = current_user.contacts.find_by(id: params[:id])
        end
        desc 'Fetch details of a particaular contact'
        get '/', serializer: DetailedContactSerializer, root: 'contact' do
          @contact
        end

        desc 'Delete Contact'
        delete '/' do
          @contact.try(:delete)
          respond_with @contact
        end
      end
    end
  end
end