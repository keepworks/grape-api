module V1
  class Root < Grape::API
    version 'v1'

    mount V1::AuthenticationTokensEndpoint
    mount V1::SignupsEndpoint

    group do
      before do
        authenticate_user!
      end

      mount V1::UsersEndpoint
      mount V1::ContactsEndpoint
    end
  end
end
