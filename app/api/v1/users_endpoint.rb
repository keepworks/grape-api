module V1
  class UsersEndpoint < BaseEndpoint
    resource :users do
      desc 'Returns currently logged in user'
      get '/me' do
        current_user
      end

      desc 'Change password'
      params do
        requires :user, type: Hash do
          requires :current_password, allow_blank: false
          requires :password, allow_blank: false
          requires :password_confirmation, allow_blank: false
        end
      end
      post '/change_password' do
        if current_user.valid_password?(params[:user][:current_password])
          current_user.update_attributes(UserParameters.new(clean_params).permit(:change_password))
          respond_with current_user
        else
          unauthorized!
        end
      end

      desc 'Update Profile'
      params do
        requires :user, type: Hash do
          optional :first_name, allow_blank: false
          optional :last_name, allow_blank: true
          optional :email, allow_blank: false
          optional :dob, type: Date, allow_blank: true
        end
      end
      post '/update_profile' do
        current_user.update_attributes(UserParameters.new(clean_params).permit)
        respond_with current_user
      end

      desc 'Update Current User information'
      params do
        requires :user, type: Hash do
          optional :dob, type: Date
          optional :first_name, type: String
          optional :last_name, type: String
          optional :email, type: String

          optional :email_addresses_attributes, type: Array do
            optional :id, type: Integer
            optional :_destroy, type: Boolean
            requires :value, type: String
            requires :kind, type: String, values: Contactable::KINDS, default: Contactable::KINDS[0]
          end

          optional :phone_number_attributes, type: Array do
            optional :id, type: Integer
            optional :_destroy, type: Boolean
            requires :country_code, type: String
            requires :number, type: String
            requires :kind, type: String, values: Contactable::KINDS, default: Contactable::KINDS[0]
          end

          optional :postal_addresses_attributes, type: Array do
            optional :id, type: Integer
            requires :address_line1, type: String
            optional :address_line2, type: String
            requires :city, type: String
            requires :state, type: String
            requires :country, type: String
            requires :postal_code, type: String
            requires :kind, type: String, values: Contactable::KINDS, default: Contactable::KINDS[0]
            optional :_destroy, type: Boolean
          end
        end
      end
      put '/', serializer: DetailedUserSerializer, root: 'user' do
        current_user.update_attributes(UserParameters.new(clean_params).permit(:contact_information))
        respond_with current_user
      end
    end
  end
end