module V1
  class SignupsEndpoint < BaseEndpoint
    resource :signup do
      desc 'Signup with a new user'
      params do
        requires :email
        requires :password
        requires :password_confirmation
        requires :first_name, regexp: /\A[aA-zZ]+\z/
        optional :last_name, regexp: /\A[a-z]+\z/
        optional :dob, type: Date
      end
      post '/' do
        respond_with User.create(SignupParameters.new(clean_params).permit)
      end
    end
  end
end