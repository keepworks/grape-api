class UserParameters < ActionParameter::Base
  def permit(type = nil)
    permitted_attributes = default_attributes
    permitted_attributes = change_password_attributes if type == :change_password
    permitted_attributes += create_attributes if type == :create
    permitted_attributes += contactable_attributes if type == :contact_information
    params.require(:user).permit(permitted_attributes)
  end

  def default_attributes
    [:first_name, :last_name, :email, :dob]
  end

  def create_attributes
    [:password, :password_confirmation]
  end

  def change_password_attributes
    [:password, :password_confirmation]
  end

  def contactable_attributes
    [
      email_addresses_attributes: [:id, :value, :kind, :_destroy],
      phone_numbers_attributes: [:id, :country_code, :number, :kind, :_destroy],
      postal_addresses_attributes: [:id, :address_line1, :address_line2, :city, :state, :country, :postal_code, :kind, :_destroy]
    ]
  end
end
