class SignupParameters < ActionParameter::Base
  def permit
    params.permit(default_attributes)
  end

  def default_attributes
    [:first_name, :last_name, :email, :dob, :password, :password_confirmation]
  end
end
