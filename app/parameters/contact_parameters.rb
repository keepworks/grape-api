class ContactParameters < ActionParameter::Base
  def permit(type = nil)
    permitted_attributes = default_attributes

    params.require(:contact).permit(permitted_attributes)
  end

  def default_attributes
    [
      :first_name, :last_name, :picture, :dob,
      email_addresses_attributes: [:id, :value, :kind, :_destroy],
      phone_numbers_attributes: [:id, :country_code, :number, :kind, :_destroy],
      postal_addresses_attributes: [:id, :address_line1, :address_line2, :city, :state, :country, :postal_code, :kind, :_destroy]
    ]
  end
end
