class UserSerializer < BaseSerializer
  attributes :email, :first_name, :last_name, :dob
end
