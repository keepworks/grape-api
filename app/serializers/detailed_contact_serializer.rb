class DetailedContactSerializer < ContactSerializer
  has_many :phone_numbers
  has_many :email_addresses
  has_many :postal_addresses
end
