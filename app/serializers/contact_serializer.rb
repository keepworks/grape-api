class ContactSerializer < BaseSerializer
  attributes :first_name, :last_name, :dob, :picture
end
