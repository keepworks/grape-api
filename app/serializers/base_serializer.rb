class BaseSerializer < ActiveModel::Serializer
  embed :ids, include: true

  attributes :id, :created_at, :updated_at
end
