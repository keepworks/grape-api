class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :contacts, dependent: :destroy
  has_many :authentication_tokens, dependent: :destroy
  has_many :email_addresses, as: :contactable, dependent: :destroy
  has_many :phone_numbers, as: :contactable, dependent: :destroy
  has_many :postal_addresses, as: :contactable, dependent: :destroy

  accepts_nested_attributes_for :email_addresses, allow_destroy: true
  accepts_nested_attributes_for :phone_numbers, allow_destroy: true
  accepts_nested_attributes_for :postal_addresses, allow_destroy: true
end
