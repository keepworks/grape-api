class EmailAddress < ActiveRecord::Base
  include Contactable

  validates :kind, presence: true
end
