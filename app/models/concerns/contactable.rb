module Contactable
  extend ActiveSupport::Concern
  KINDS = %w(mobile home work personal fax other)

  included do
    string_enum kind: KINDS

    belongs_to :contactable, polymorphic: true
  end
end
