require 'carrierwave/orm/activerecord'

CarrierWave.configure do |config|
  if Rails.env.production? || Rails.env.staging?
    config.fog_credentials = {
      provider: 'AWS',
      aws_access_key_id: ENV['AWS_ACCESS_KEY_ID'],
      aws_secret_access_key: ENV['AWS_SECRET_ACCESS_KEY'],
      persistent: false,
      region: ENV['FOG_REGION']
    }
    config.fog_directory = ENV['FOG_DIRECTORY']
    config.storage :fog
    config.remove_previously_stored_files_after_update = true
  else
    config.storage = :file
  end

  if Rails.env.test?
    config.enable_processing = false
  end
end

module CarrierWave
  module MiniMagick
    def fix_exif_rotation
      manipulate! do |img|
        img.tap(&:auto_orient)
        img = yield(img) if block_given?
        img
      end
    end
  end
end
