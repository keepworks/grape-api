module StringEnum
  extend ActiveSupport::Concern
  module ClassMethods
    def string_enum(attribute, mapping = {}, options = {})
      pluralized_attribute = attribute.to_s.pluralize

      if options[:prefix]
        if options[:prefix].is_a? String
          prefix = options[:prefix]
        else
          prefix = "#{attribute}_"
        end
      else
        prefix = ''
      end

      define_singleton_method pluralized_attribute do
        mapping
      end

      define_method "#{attribute}_human_readable" do
        mapping[send(attribute)]
      end

      mapping.each do |key, value|
        define_method "#{prefix}#{key}!" do
          update_attribute(attribute, key)
        end

        define_method "#{prefix}#{key}?" do
          send(attribute) == key
        end

        define_singleton_method "#{prefix}#{key}" do
          where("#{attribute} = ?", key)
        end
      end
    end
  end
end

ActiveRecord::Base.send(:include, StringEnum)
