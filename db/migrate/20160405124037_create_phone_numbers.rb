class CreatePhoneNumbers < ActiveRecord::Migration
  def change
    create_table :phone_numbers do |t|
      t.references :contactable, polymorphic: true, index: true
      t.string :country_code
      t.string :number
      t.string :kind

      t.timestamps null: false
    end
  end
end
