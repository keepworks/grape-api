class CreateEmailAddresses < ActiveRecord::Migration
  def change
    create_table :email_addresses do |t|
      t.references :contactable, polymorphic: true, index: true
      t.string :value
      t.string :kind

      t.timestamps null: false
    end
  end
end
