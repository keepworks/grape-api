class CreatePostalAddresses < ActiveRecord::Migration
  def change
    create_table :postal_addresses do |t|
      t.references :contactable, polymorphic: true, index: true
      t.string :address_line1
      t.string :address_line2
      t.string :city
      t.string :state
      t.string :country
      t.string :postal_code
      t.string :kind

      t.timestamps null: false
    end
  end
end
