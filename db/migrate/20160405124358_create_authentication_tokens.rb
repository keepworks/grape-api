class CreateAuthenticationTokens < ActiveRecord::Migration
  def change
    create_table :authentication_tokens do |t|
      t.references :user, index: true
      t.string :digest
      t.datetime :expires_at

      t.timestamps
    end
  end
end
