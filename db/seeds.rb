# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
DEFAULT_PASSWORD = 'qwerqwer'

10.times do
  user = User.create({
    email: FFaker::Internet.email,
    first_name: FFaker::Name.first_name,
    last_name: FFaker::Name.last_name,
    dob: rand(6200..16800).days.ago,
    password: DEFAULT_PASSWORD,
    password_confirmation: DEFAULT_PASSWORD
  })

  rand(1..2).times do
    user.email_addresses.create({
      value: FFaker::Internet.email,
      kind: Contactable::KINDS.sample
    })

    country = FFaker::Address.country
    country_code = FFaker::Address.country_code(country)
    user.phone_numbers.create({
      country_code: country_code,
      number: FFaker::PhoneNumber.short_phone_number,
      kind: Contactable::KINDS.sample
    })

    user.postal_addresses.create({
      address_line1: FFaker::Address.street_address,
      address_line2: FFaker::Address.secondary_address,
      city: FFaker::Address.city,
      state: FFaker::AddressUS.state,
      country: country
    })
  end

  rand(5..10).times do
    contact = user.contacts.create({
      first_name: FFaker::Name.first_name,
      last_name: FFaker::Name.last_name,
      dob: rand(6200..16800).days.ago
    })

    contact.email_addresses.create({
      value: FFaker::Internet.email,
      kind: Contactable::KINDS.sample
    })

    country = FFaker::Address.country
    country_code = FFaker::Address.country_code(country)
    contact.phone_numbers.create({
      country_code: country_code,
      number: FFaker::PhoneNumber.short_phone_number,
      kind: Contactable::KINDS.sample
    })

    contact.postal_addresses.create({
      address_line1: FFaker::Address.street_address,
      address_line2: FFaker::Address.secondary_address,
      city: FFaker::Address.city,
      state: FFaker::AddressUS.state,
      country: country
    })
  end
end